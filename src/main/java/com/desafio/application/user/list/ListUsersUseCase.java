package com.desafio.application.user.list;

import com.desafio.application.user.IUserEntity;
import com.desafio.application.user.IUserGateway;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.util.ArrayList;

import static com.desafio.shared.utils.ActionConstants.FIND_ALL_USERS;
import static com.desafio.shared.utils.SuccessConstants.FOUND_X_USERS;

public class ListUsersUseCase {

  private final IUserGateway<IUserEntity, IUserEntity> iUserGateway;

  private static final ILog log = new Log(ListUsersUseCase.class);

  private ListUsersUseCase(IUserGateway iUserGateway) {
    this.iUserGateway = iUserGateway;
  }

  public static ListUsersUseCase of(IUserGateway iUserGateway) {
    return new ListUsersUseCase(iUserGateway);
  }

  public Pagination<ListUsersOutput> execute(SearchQuery searchQuery) {
    log.info(FIND_ALL_USERS);
    try {
      Pagination<ListUsersOutput> outputPagination = this.iUserGateway.findAll(searchQuery).map(ListUsersOutput::of);
      log.info(FOUND_X_USERS, outputPagination.items().size());
      return outputPagination;
    } catch (Exception e) {
      log.error(e.getMessage());
      return new Pagination<>(searchQuery.page(), searchQuery.perPage(), 0, new ArrayList<>());
    }
  }
}
