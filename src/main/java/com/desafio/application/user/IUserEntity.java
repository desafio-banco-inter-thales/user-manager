package com.desafio.application.user;

import com.desafio.shared.notification.INotification;

import java.time.Instant;

public interface IUserEntity {
  String name();
  String cpf();
  String id();
  Boolean isAdm();

  Instant createdAt();
  Instant updatedAt();
  Instant deletedAt();
  INotification notification();

  String toString();
}
