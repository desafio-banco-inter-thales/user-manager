package com.desafio.application.user;

import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;

import java.util.List;
import java.util.Optional;

public interface IUserGateway<I extends IUserEntity, O extends IUserEntity> {
  Optional<O> findByCpf(String cpf);
  O save(I user);

  Pagination<O> findAll(SearchQuery searchQuery);
}
