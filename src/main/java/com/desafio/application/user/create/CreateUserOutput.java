package com.desafio.application.user.create;

import com.desafio.application.user.IUserEntity;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;

import java.time.Instant;

import static com.desafio.shared.utils.Constants.USER_STR;

public record CreateUserOutput(String id, String name, String cpf, Boolean isAdm, Instant createdAt, Instant updatedAt, Instant deletedAt, INotification notificationErrors) {

  public static CreateUserOutput of(IUserEntity user) {
    return new CreateUserOutput(user.id(), user.name(), user.cpf(), user.isAdm(), user.createdAt(), user.updatedAt(), user.deletedAt(), null);
  }

  public static CreateUserOutput of(INotification notification) {
    return new CreateUserOutput(null, null, null, null, null, null, null, notification);
  }

  public static CreateUserOutput of() {
    return new CreateUserOutput(null, null, null, null, null, null, null, null);
  }

  public static CreateUserOutput of(String message) {
    INotification notification = new Notification();
    notification.append(message, USER_STR);
    return new CreateUserOutput(null, null, null, null, null, null, null, notification);
  }
}