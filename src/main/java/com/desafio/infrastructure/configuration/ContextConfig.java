package com.desafio.infrastructure.configuration;

import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.application.user.list.ListUsersUseCase;
import com.desafio.infrastructure.gateway.user.UserSqlGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContextConfig {

  private final UserSqlGateway userGateway;

  public ContextConfig(UserSqlGateway userGateway) {
    this.userGateway = userGateway;
  }

  @Bean
  public FindUserByCpfUseCase findUserByCpfUseCase() {
    return FindUserByCpfUseCase.of(this.userGateway);
  }

  @Bean
  public CreateUserUseCase createUserUseCase() {
    return CreateUserUseCase.of(this.userGateway, findUserByCpfUseCase());
  }

  @Bean
  public ListUsersUseCase listUsersUseCase() {
    return ListUsersUseCase.of(this.userGateway);
  }
}
