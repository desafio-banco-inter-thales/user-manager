package com.desafio.infrastructure.gateway.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserJpaEntity, String> {

  Optional<UserJpaEntity> findByCpf(String cpf);

  Page<UserJpaEntity> findAll(Specification<UserJpaEntity> whereClause, Pageable page);

}
