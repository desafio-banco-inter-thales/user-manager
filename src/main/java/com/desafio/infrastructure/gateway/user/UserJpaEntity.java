package com.desafio.infrastructure.gateway.user;

import com.desafio.application.user.IUserEntity;
import com.desafio.application.user.UserDto;
import com.desafio.shared.notification.INotification;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "user")
public class UserJpaEntity implements IUserEntity {

  @Id
  private String id;

  @Column(name = "cpf", length = 11, nullable = false)
  private String cpf;

  @Column(name = "name", length = 11, nullable = false)
  private String name;

  @Column(name = "is_adm", length = 11, nullable = false)
  private Boolean isAdm;

  @Column(
          name = "created_at",
          columnDefinition = "DATETIME(6)",
          nullable = false
  )
  private Instant created_at;

  @Column(
          name = "updated_at",
          columnDefinition = "DATETIME(6)",
          nullable = false
  )
  private Instant updated_at;

  @Column(
          name = "deleted_at",
          columnDefinition = "DATETIME(6)",
          nullable = true
  )
  private Instant deleted_at;

  private UserJpaEntity() {
  }

  private UserJpaEntity(String id, String cpf, String name, Boolean isAdm, Instant created_at, Instant updated_at, Instant deleted_at) {
    this.id = id;
    this.cpf = cpf;
    this.name = name;
    this.isAdm = isAdm;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.deleted_at = deleted_at;
  }

  public static UserJpaEntity of(IUserEntity iUserEntity) {
    return new UserJpaEntity(
            iUserEntity.id(),
            iUserEntity.cpf(),
            iUserEntity.name(),
            iUserEntity.isAdm(),
            iUserEntity.createdAt(),
            iUserEntity.updatedAt(),
            iUserEntity.deletedAt());
  }

  public UserDto toAggregate() {
    return UserDto.of(this);
  }

  @Override
  public String name() {
    return this.name;
  }

  @Override
  public String cpf() {
    return this.cpf;
  }

  @Override
  public String id() {
    return this.id;
  }

  @Override
  public Boolean isAdm() {
    return this.isAdm;
  }

  @Override
  public Instant createdAt() {
    return this.created_at;
  }

  @Override
  public Instant updatedAt() {
    return this.updated_at;
  }

  @Override
  public Instant deletedAt() {
    return this.deleted_at;
  }

  @Override
  public INotification notification() {
    return null;
  }

  @Override
  public String toString() {
    return "UserJpaEntity{" +
            "id='" + id + '\'' +
            ", cpf='" + cpf + '\'' +
            ", name='" + name + '\'' +
            ", isAdm=" + isAdm +
            ", created_at=" + created_at +
            ", updated_at=" + updated_at +
            ", deleted_at=" + deleted_at +
            '}';
  }
}
