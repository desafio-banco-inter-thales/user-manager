package com.desafio.infrastructure.gateway.user;

import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.UserDto;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserSqlGateway implements IUserGateway<UserDto, UserJpaEntity> {

  private final UserRepository userRepository;

  public UserSqlGateway(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public Optional<UserJpaEntity> findByCpf(String cpf) {
    final var optionalUserJpaEntity = this.userRepository.findByCpf(cpf);

    if(optionalUserJpaEntity.isEmpty())
      return Optional.empty();

    return Optional.of(optionalUserJpaEntity.get());
  }

  @Override
  public UserJpaEntity save(UserDto user) {
    return this.userRepository.save(UserJpaEntity.of(user));
  }

  @Override
  public Pagination<UserJpaEntity> findAll(SearchQuery searchQuery) {
    final var page = PageRequest.of(
            searchQuery.page(),
            searchQuery.perPage()
    );

    final var pageResult =
            this.userRepository.findAll(page);

    return new Pagination<>(
            pageResult.getNumber(),
            pageResult.getSize(),
            pageResult.getTotalElements(),
            pageResult.toList()
    );
  }
}
