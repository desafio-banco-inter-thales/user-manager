package com.desafio.infrastructure.api.service;

import com.desafio.application.user.create.CreateUserCommand;
import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.infrastructure.stream.event.UserEvent;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

@Service
public class CreateUserService {

  private final CreateUserUseCase useCase;

  private final EventProducer eventProducer;

  private static final ObjectMapper mapper = new ObjectMapper();

  private static final ILog log = new Log(CreateUserService.class);

  public CreateUserService(CreateUserUseCase useCase, EventProducer eventProducer) {
    this.useCase = useCase;
    this.eventProducer = eventProducer;
  }

  public CreateUserOutput execute(String name, String cpf, Boolean isAdm) throws JsonProcessingException {
    final var command = CreateUserCommand.of(name, cpf, isAdm);
    final var response = this.useCase.execute(command);

    if (response.notificationErrors() != null)
      return response;

//    log.info(SENDING_EVENT);
    UserEvent event = UserEvent.of(response.cpf(), response.name(), response.isAdm());

    this.eventProducer.produce(mapper.writeValueAsString(event));

    return response;
  }
}
