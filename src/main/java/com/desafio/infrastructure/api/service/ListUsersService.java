package com.desafio.infrastructure.api.service;

import com.desafio.application.user.list.ListUsersOutput;
import com.desafio.application.user.list.ListUsersUseCase;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import org.springframework.stereotype.Service;

@Service
public class ListUsersService {

  private final ListUsersUseCase listUsersUseCase;

  public ListUsersService(ListUsersUseCase listUsersUseCase) {
    this.listUsersUseCase = listUsersUseCase;
  }

  public Pagination<ListUsersOutput> execute(SearchQuery searchQuery) {
    return listUsersUseCase.execute(searchQuery);
  }
}
