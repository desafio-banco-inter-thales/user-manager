package com.desafio.infrastructure.api.pagination;

public record SearchQuery(
        int page,
        int perPage
) {
}
