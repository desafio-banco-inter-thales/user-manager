package com.desafio.infrastructure.stream.event;

import com.desafio.application.user.IUserEntity;

public record UserEvent(String cpf, String name, Boolean isAdm) {

  public static UserEvent of(String cpf, String name, Boolean isAdm) {
    return new UserEvent(cpf, name, isAdm);
  }

}