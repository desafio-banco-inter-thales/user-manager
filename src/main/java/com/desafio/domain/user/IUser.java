package com.desafio.domain.user;

import com.desafio.shared.notification.INotification;

import java.time.Instant;

public interface IUser {
  String getIdStr();
  String getCpfStr();
  INotification getNotification();
  Instant getCreatedAt();
  Instant getUpdatedAt();
  Instant getDeletedAt();
  Boolean getAdm();
  String getName();
}
