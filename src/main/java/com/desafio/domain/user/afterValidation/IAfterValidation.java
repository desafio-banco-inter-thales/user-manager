package com.desafio.domain.user.afterValidation;

public interface IAfterValidation<T> {
  void execute(T object);
}
