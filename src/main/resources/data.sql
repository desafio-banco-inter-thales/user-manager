CREATE SCHEMA IF NOT EXISTS desafio;

CREATE TABLE IF NOT EXISTS `user`(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    cpf CHAR(11) NOT NULL,
    name VARCHAR(30) NOT NULL,
    is_adm BIT DEFAULT 1,
    created_at DATETIME(6) NOT NULL,
    updated_at DATETIME(6) NOT NULL,
    deleted_at DATETIME(6)
);

--INSERT INTO `user` (id, cpf, name, is_adm, created_at, updated_at, deleted_at) VALUES('1', '84034668032', 'Thales', true, now(), now(), null);