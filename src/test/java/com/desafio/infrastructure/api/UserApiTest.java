package com.desafio.infrastructure.api;

import com.desafio.ControllerTest;
import com.desafio.application.user.UserDto;
import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.application.user.find.FindUserByCpfOutput;
import com.desafio.application.user.list.ListUsersOutput;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.api.UserApi;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.request.CreateUserRequest;
import com.desafio.infrastructure.api.service.CreateUserService;
import com.desafio.infrastructure.api.service.FindUserByCpfService;
import com.desafio.infrastructure.api.service.ListUsersService;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Objects;

import static com.desafio.shared.utils.Constants.*;
import static com.desafio.shared.utils.ErrorConstants.NAME_LENGTH_INVALID;
import static com.desafio.shared.utils.ErrorConstants.STRING_SHOULD_NOT_BE_NULL;
import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static com.desafio.shared.utils.Samples.USER_NAME_SAMPLE;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ControllerTest(controllers = UserApi.class)
public class UserApiTest {

  private static final String ROUTE_USERS = "/users";
  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper mapper;

  @MockBean
  private CreateUserService createUserService;

  @MockBean
  private FindUserByCpfService findUserByCPFService;

  @MockBean
  private ListUsersService listUsersService;

  @MockBean
  private EventProducer eventProducer;

  @Test
  public void givenAValidCommand_whenCallsCreateUser_shouldReturnUserId() throws Exception {

    final var requestBody = CreateUserRequest.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);

    final var user = User.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);
    final var mockedUser = UserDto.of(user);

    when(createUserService.execute(any(), any(), any()))
            .thenReturn(CreateUserOutput.of(mockedUser));

    final var request = post(ROUTE_USERS)
            .contentType(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(requestBody));

    final var userLocation = ROUTE_USERS.concat("/").concat(mockedUser.cpf());
    final var response = this.mvc.perform(request)
            .andDo(print())
            .andExpectAll(
                    status().isCreated(),
                    header().string("Location", userLocation)
            );


    response.andExpect(status().isCreated())
            .andExpect(header().string("Location", userLocation))
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.cpf", equalTo(CPF_SAMPLE)));

    verify(createUserService, times(1)).execute(any(), any(), any());
  }

  @Test
  public void givenAInvalidCommandNullName_whenCallsCreateUser_shouldReturnError() throws Exception {

    final var requestBody = CreateUserRequest.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);

    final var mockedNotificationErrors = new Notification();
    mockedNotificationErrors.append(new NotificationErrorProps(STRING_SHOULD_NOT_BE_NULL.replace("{}", NAME_STR), COMPANY_STR));

    when(createUserService.execute(any(), any(), any()))
            .thenReturn(CreateUserOutput.of(mockedNotificationErrors));

    final var request = post(ROUTE_USERS)
            .contentType(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(requestBody));

    this.mvc.perform(request)
            .andDo(print())
            .andExpect(status().isUnprocessableEntity())
            .andExpect(header().string("Location", nullValue()))
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE));

    verify(createUserService, times(1)).execute(any(), any(), any());
  }

  @Test
  public void givenAValidCommand_whenCallsFindUserByCPF_shouldReturnUser() throws Exception {

    final var user = User.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);
    final var mockedUser = UserDto.of(user);

    when(findUserByCPFService.execute(any()))
            .thenReturn(FindUserByCpfOutput.of(mockedUser));

    final var request = get(ROUTE_USERS.concat("/").concat(CPF_SAMPLE));

    final var response = this.mvc.perform(request)
            .andDo(print())
            .andExpectAll(
                    status().isOk()
            );

    response.andExpect(status().isOk())
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.cpf", equalTo(CPF_SAMPLE)));

    verify(findUserByCPFService, times(1)).execute(any());
  }

  @Test
  public void givenAInvalidCommandNameLength_whenCallsCreateUser_shouldReturnError() throws Exception {

    final var requestBody = CreateUserRequest.of("Fi ", CPF_SAMPLE, true);

    final var mockedNotificationErrors = new Notification();
    mockedNotificationErrors.append(new NotificationErrorProps(NAME_LENGTH_INVALID, USER_STR));

    when(createUserService.execute(any(), any(), any()))
            .thenReturn(CreateUserOutput.of(mockedNotificationErrors));

    final var request = post(ROUTE_USERS)
            .contentType(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(requestBody));

    this.mvc.perform(request)
            .andDo(print())
            .andExpect(status().isUnprocessableEntity())
            .andExpect(header().string("Location", nullValue()))
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.notificationErrors.errors[0].message", equalTo(NAME_LENGTH_INVALID)));
  }

  @Test
  public void givenValidParams_whenCallsListCategories_shouldReturnCategories() throws Exception {

    final var user = User.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);
    final var mockedUser = UserDto.of(user);

    final var expectedPage = 0;
    final var expectedPerPage = 10;
    final var expectedItemsCount = 1;
    final var expectedTotal = 1;

    final var expectedItems = List.of(ListUsersOutput.of(mockedUser));

    when(listUsersService.execute(any()))
            .thenReturn(new Pagination<>(expectedPage, expectedPerPage, expectedTotal, expectedItems));

    // when
    final var request = get(ROUTE_USERS)
            .queryParam("page", String.valueOf(expectedPage))
            .queryParam("perPage", String.valueOf(expectedPerPage))
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON);

    final var response = this.mvc.perform(request)
            .andDo(print());

    // then
    response.andExpect(status().isOk())
            .andExpect(jsonPath("$.total", equalTo(expectedTotal)))
            .andExpect(jsonPath("$.items", hasSize(expectedItemsCount)))
            .andExpect(jsonPath("$.items[0].id", equalTo(mockedUser.id())))
            .andExpect(jsonPath("$.items[0].name", equalTo(mockedUser.name())))
            .andExpect(jsonPath("$.items[0].isAdm", equalTo(mockedUser.isAdm())))
            .andExpect(jsonPath("$.items[0].createdAt", equalTo(mockedUser.createdAt().toString())))
            .andExpect(jsonPath("$.items[0].updatedAt", equalTo(mockedUser.updatedAt().toString())));

    verify(listUsersService, times(1)).execute(argThat(query ->
            Objects.equals(expectedPage, query.page())
                    && Objects.equals(expectedPerPage, query.perPage())
    ));
  }
}
