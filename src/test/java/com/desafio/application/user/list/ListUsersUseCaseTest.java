package com.desafio.application.user.list;

import com.desafio.application.user.IUserEntity;
import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.UserDto;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.desafio.shared.utils.Samples.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ListUsersUseCaseTest {

  @InjectMocks
  private ListUsersUseCase useCase;

  @Mock
  private IUserGateway<IUserEntity, UserDto> companyGateway;

  @Test
  public void givenAValidQuery_whenCallsListUsers_thenShouldReturnUsers() {
    final var users = List.of(
            UserDto.of(User.of(USER_NAME_SAMPLE, CPF_SAMPLE, true)),
            UserDto.of(User.of("User 2", "84034668032", false))
    );

    final var expectedPage = 0;
    final var expectedPerPage = 10;

    final var aQuery =
            new SearchQuery(expectedPage, expectedPerPage);

    final var expectedPagination =
            new Pagination<>(expectedPage, expectedPerPage, users.size(), users);

    final var expectedItemsCount = 2;
    final var expectedResult = expectedPagination.map(ListUsersOutput::of);

    when(companyGateway.findAll(eq(aQuery)))
            .thenReturn(expectedPagination);

    final var actualResult = useCase.execute(aQuery);

    assertEquals(expectedItemsCount, actualResult.items().size());
    assertEquals(expectedResult, actualResult);
    assertEquals(expectedPage, actualResult.currentPage());
    assertEquals(expectedPerPage, actualResult.perPage());
    assertEquals(users.size(), actualResult.total());
  }

  @Test
  public void givenAValidQuery_whenHasNoResults_thenShouldReturnEmptyUsers() {
    final var users = List.<UserDto>of();

    final var expectedPage = 0;
    final var expectedPerPage = 10;

    final var aQuery =
            new SearchQuery(expectedPage, expectedPerPage);

    final var expectedPagination =
            new Pagination<>(expectedPage, expectedPerPage, users.size(), users);

    final var expectedItemsCount = 0;
    final var expectedResult = expectedPagination.map(ListUsersOutput::of);

    when(companyGateway.findAll(eq(aQuery)))
            .thenReturn(expectedPagination);

    final var actualResult = useCase.execute(aQuery);

    Assertions.assertEquals(expectedItemsCount, actualResult.items().size());
    Assertions.assertEquals(expectedResult, actualResult);
    Assertions.assertEquals(expectedPage, actualResult.currentPage());
    Assertions.assertEquals(expectedPerPage, actualResult.perPage());
    Assertions.assertEquals(users.size(), actualResult.total());
  }

  @Test
  public void givenAValidQuery_whenGatewayThrowsException_shouldSuppressException() {
    final var expectedPage = 0;
    final var expectedPerPage = 10;
    final var expectedErrorMessage = "Gateway error";

    final var aQuery =
            new SearchQuery(expectedPage, expectedPerPage);

    final var expectedPagination =
            new Pagination<>(expectedPage, expectedPerPage, 0, new ArrayList<UserDto>());

    final var expectedItemsCount = 0;
    final var expectedResult = expectedPagination.map(ListUsersOutput::of);

    when(companyGateway.findAll(eq(aQuery)))
            .thenThrow(new IllegalStateException(expectedErrorMessage));

    final var actualResult = useCase.execute(aQuery);

    Assertions.assertEquals(expectedItemsCount, actualResult.items().size());
    Assertions.assertEquals(expectedResult, actualResult);
    Assertions.assertEquals(expectedPage, actualResult.currentPage());
    Assertions.assertEquals(expectedPerPage, actualResult.perPage());
    Assertions.assertEquals(0, actualResult.total());
  }

}