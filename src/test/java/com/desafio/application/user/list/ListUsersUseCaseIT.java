package com.desafio.application.user.list;

import com.desafio.application.user.AbstractIT;
import com.desafio.application.user.UserDto;
import com.desafio.application.user.create.CreateUserCommand;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static com.desafio.shared.utils.Samples.USER_NAME_SAMPLE;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ListUsersUseCaseIT extends AbstractIT {

  @Autowired
  CreateUserUseCase createUserUseCase;

  @Autowired
  ListUsersUseCase listUsersUseCase;

  @Test
  public void givenAValidQuery_whenCallsListCategories_thenShouldReturnCompaniesFromGateway() {
    final var users = List.of(
            UserDto.of(User.of(USER_NAME_SAMPLE, CPF_SAMPLE, true)),
            UserDto.of(User.of("User 2", "84034668032", false))
    );

    for (UserDto userDto : users) {
      createUserUseCase.execute(CreateUserCommand.of(userDto.name(), userDto.cpf(), userDto.isAdm()));
    }

    final var expectedPage = 0;
    final var expectedPerPage = 10;

    final var aQuery =
            new SearchQuery(expectedPage, expectedPerPage);

    final var expectedItemsCount = 2;

    final var actualResult = listUsersUseCase.execute(aQuery);

    assertEquals(expectedItemsCount, actualResult.items().size());
    assertEquals(expectedPage, actualResult.currentPage());
    assertEquals(expectedPerPage, actualResult.perPage());
    assertEquals(users.size(), actualResult.total());
  }
}
