package com.desafio.application.user;

import com.desafio.IntegrationTest;
import com.desafio.infrastructure.gateway.user.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

@IntegrationTest
public abstract class AbstractIT {

  @Autowired
  protected UserRepository repository;

  @SpyBean
  protected IUserGateway userGateway;

  @BeforeEach
  void setup() {
    repository.deleteAll();
  }

  @AfterEach
  void shutdown() {
    repository.deleteAll();
  }

}
