package com.desafio.application.user.create;

import com.desafio.application.user.AbstractIT;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static com.desafio.shared.utils.Samples.USER_NAME_SAMPLE;

public class CreateUserUseCaseIT extends AbstractIT {

  @Autowired
  CreateUserUseCase useCase;

  @Test
  void givenValidCommand_whenCallsCreateUser_shouldReturnUserId() {

    Assertions.assertEquals(0, repository.count());

    final var aCommand =
            CreateUserCommand.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);

    final var actualOutput = useCase.execute(aCommand);

    Assertions.assertNotNull(actualOutput);
    Assertions.assertEquals(CPF_SAMPLE, actualOutput.cpf());

    Assertions.assertEquals(1, repository.count());

    final var actualUser =
            repository.findByCpf(actualOutput.cpf()).get();

    Assertions.assertEquals(USER_NAME_SAMPLE, actualUser.name());
    Assertions.assertEquals(CPF_SAMPLE, actualUser.cpf());
    Assertions.assertNotNull(actualUser.createdAt());
    Assertions.assertNotNull(actualUser.id());
  }

}