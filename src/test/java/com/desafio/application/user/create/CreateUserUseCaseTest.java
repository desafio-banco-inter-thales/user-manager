package com.desafio.application.user.create;

import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.UserDto;
import com.desafio.application.user.find.FindUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfOutput;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.domain.user.cpf.Cpf;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Objects;

import static com.desafio.shared.utils.ErrorConstants.GATEWAY_ERROR_STR;
import static com.desafio.shared.utils.ErrorConstants.USER_ALREADY_EXISTS;
import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static com.desafio.shared.utils.Samples.USER_NAME_SAMPLE;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateUserUseCaseTest {

  @Mock
  private IUserGateway userGateway;

  @Mock
  private FindUserByCpfUseCase findUserByCpfUseCase;

  @InjectMocks
  private CreateUserUseCase useCase;

  @Test
  void givenValidCommand_whenCallsCreateCategory_shouldReturnCategoryId() {
    final var command = CreateUserCommand.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);

    FindUserByCpfOutput findUserByCpfOutput = FindUserByCpfOutput.of("");
    when(findUserByCpfUseCase.execute(new FindUserByCpfCommand(CPF_SAMPLE))).thenReturn(findUserByCpfOutput);

    when(userGateway.save(any())).thenAnswer(returnsFirstArg());

    final CreateUserOutput output = useCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertNotNull(output.cpf());

    verify(userGateway, times(1)).save(any(UserDto.class));
  }

  @Test
  void givenValidCommandAndExistentUser_whenCallsCreateCategory_shouldReturnCategoryId() {
    final var command = CreateUserCommand.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);
    final var expectedMessage = "[USER]: "+USER_ALREADY_EXISTS.replace("{}", CPF_SAMPLE);

    UserDto userDto = UserDto.of("1", CPF_SAMPLE, USER_NAME_SAMPLE, null);
    FindUserByCpfOutput findUserByCpfOutput = FindUserByCpfOutput.of(userDto);

    when(findUserByCpfUseCase.execute(new FindUserByCpfCommand(CPF_SAMPLE))).thenReturn(findUserByCpfOutput);

    final var createUser = useCase.execute(command);

    Assertions.assertNotNull(createUser.notificationErrors());
    Assertions.assertEquals(createUser.notificationErrors().messages(""), expectedMessage);
  }

  @Test
  void givenInvalidBlankName_whenCallsCreateCategory_shouldThrowAnError() {
    final String expected_error_message = "[USER]: 'NAME' SHOULD NOT BE BLANK.[USER]: 'NAME' MUST BE BETWEEN 3 AND 30 CHARACTERS.";

    final var command = CreateUserCommand.of("", CPF_SAMPLE, true);
    final var output = useCase.execute(command);

    Assertions.assertEquals(expected_error_message, output.notificationErrors().messages(""));
    verify(userGateway, times(0)).save(any(UserDto.class));
  }

  @Test
  void givenValidCommand_whenGatewayThrowsAnError_shouldThrowAnError() {

    final var command = CreateUserCommand.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);

    FindUserByCpfOutput findUserByCpfOutput = FindUserByCpfOutput.of("");
    when(findUserByCpfUseCase.execute(new FindUserByCpfCommand(CPF_SAMPLE))).thenReturn(findUserByCpfOutput);

    when(userGateway.save(any())).thenThrow(new RuntimeException(GATEWAY_ERROR_STR));

    final var output = useCase.execute(command);

    Assertions.assertNotNull(output.notificationErrors());
    verify(userGateway, times(1)).save(any(UserDto.class));
  }
}
