package com.desafio.application.user.find;

import com.desafio.application.user.AbstractIT;
import com.desafio.application.user.UserDto;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.gateway.user.UserJpaEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static com.desafio.shared.utils.Samples.USER_NAME_SAMPLE;

public class FindByCpfUseCaseIT extends AbstractIT {

  @Autowired
  FindUserByCpfUseCase useCase;

  @Test
  void givenValidCommand_whenCallsFindByCPF_shouldReturnUser() {

    Assertions.assertEquals(0, repository.count());


    final var user = User.of("1", USER_NAME_SAMPLE, CPF_SAMPLE, true);
    final var userDto = UserDto.of(user);

    final var userOutput = this.repository.save(UserJpaEntity.of(userDto));

    Assertions.assertNotNull(userOutput);
    Assertions.assertNotNull(userOutput.id());

    Assertions.assertEquals(1, repository.count());

    final var aCommand =
            FindUserByCpfCommand.of(CPF_SAMPLE);

    final var actualUser = useCase.execute(aCommand);

    Assertions.assertEquals(USER_NAME_SAMPLE, actualUser.name());
    Assertions.assertEquals(CPF_SAMPLE, actualUser.cpf());
    Assertions.assertNull(actualUser.notificationErrors());
  }

}