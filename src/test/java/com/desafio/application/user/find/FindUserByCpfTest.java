package com.desafio.application.user.find;

import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.UserDto;
import com.desafio.domain.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.desafio.shared.utils.Constants.USER_STR;
import static com.desafio.shared.utils.ErrorConstants.CPF_INVALID;
import static com.desafio.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static com.desafio.shared.utils.Samples.USER_NAME_SAMPLE;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FindUserByCpfTest {

  @Mock
  private IUserGateway userGateway;

  @InjectMocks
  private FindUserByCpfUseCase findUserByCpfUseCase;

  @Test
  void givenValidCPF_whenCallsFindByCPF_shouldReturnUser() {
    final UserDto userDto = UserDto.of("1", CPF_SAMPLE, USER_NAME_SAMPLE, true);
    FindUserByCpfCommand command = FindUserByCpfCommand.of(CPF_SAMPLE);

    when(userGateway.findByCpf(anyString())).thenReturn(Optional.of(userDto));

    FindUserByCpfOutput output = findUserByCpfUseCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertEquals(USER_NAME_SAMPLE, output.name());
  }

  @Test
  void givenValidCPF_whenGatewayThrowException_shouldThrowException() {
    FindUserByCpfCommand command = FindUserByCpfCommand.of(CPF_SAMPLE);
    String expected_error_message = "[USER]: COULD NOT FIND OBJECT {}".replace("{}", command.cpf());

    when(userGateway.findByCpf(command.cpf())).thenThrow(new IllegalStateException(GATEWAY_FIND_STR));

    FindUserByCpfOutput output = findUserByCpfUseCase.execute(command);

    Assertions.assertEquals(expected_error_message, output.notificationErrors().messages(""));

    verify(userGateway, times(1)).findByCpf(command.cpf());
  }

  @Test
  void givenInvalidCPF_whenCallsFindByCPF_shouldReturnNothing() {
    FindUserByCpfCommand command = FindUserByCpfCommand.of("123");
    String expectedMessage = "["+USER_STR+"]: "+CPF_INVALID.replace("{}", "123");

    FindUserByCpfOutput output = findUserByCpfUseCase.execute(command);

    Assertions.assertEquals(expectedMessage, output.notificationErrors().messages(""));
  }

}