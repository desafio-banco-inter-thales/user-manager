package com.desafio.domain;

import com.desafio.domain.user.User;
import com.desafio.domain.user.cpf.Cpf;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.desafio.shared.utils.Constants.*;
import static com.desafio.shared.utils.ErrorConstants.NAME_LENGTH_INVALID;
import static com.desafio.shared.utils.ErrorConstants.STRING_SHOULD_NOT_BE_NULL;
import static com.desafio.shared.utils.Samples.*;

public class UserTest {

  @Test
  void givenValidParamsWithCpfWithinPunctuation_whenCallNewUser_thenShouldCreateUserWithCpfWithinPunctuation() {
    final User createdUser = User.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);
    createdUser.validate();

    Assertions.assertNotNull(createdUser);
    Assertions.assertNotNull(createdUser.getId());
    Assertions.assertNotNull(createdUser.getCreatedAt());
    Assertions.assertEquals(USER_NAME_SAMPLE, createdUser.getName());
    Assertions.assertEquals(Cpf.of(CPF_SAMPLE), createdUser.getCpf());
  }

  @Test
  void givenInvalidNullName_whenCallNewUser_thenReturnListOfErrors() {
    final Integer expected_error_count = 1;
    final String expected_error_message = STRING_SHOULD_NOT_BE_NULL.replace("{}", NAME_STR);

    User user = User.of(null, CPF_SAMPLE, true);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, user.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidBlankName_whenCallNewUser_thenReturnListOfErrors() {
    final Integer expected_error_count = 2;

    User user = User.of("", CPF_SAMPLE, true);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
  }

  @Test
  void givenInvalidNameLengthLesserThan3_whenCallNewUser_thenReturnListOfErrors() {
    final String expected_name = "Fi ";

    final String expected_error_message = NAME_LENGTH_INVALID;
    final Integer expected_error_count = 1;

    User user = User.of(expected_name, CPF_SAMPLE, true);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, user.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidNameLengthGreaterThan30_whenCallNewUser_thenReturnListOfErrors() {
    final String expected_error_message = NAME_LENGTH_INVALID;
    final Integer expected_error_count = 1;

    User user = User.of(LERO_LERO, CPF_SAMPLE, true);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, user.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenValidFormatCpfWithPunctuation_whenCallNewUserAndValidate_thenShouldCreateUserWithCpfWithinPunctuation() {
    final User createdUser = User.of(USER_NAME_SAMPLE, FORMATED_CPF_SAMPLE, true);
    createdUser.validate();

    Assertions.assertNotNull(createdUser);
    Assertions.assertNotNull(createdUser.getId());
    Assertions.assertNotNull(createdUser.getCreatedAt());
    Assertions.assertEquals(USER_NAME_SAMPLE, createdUser.getName());
    Assertions.assertEquals(0, createdUser.getNotification().getErrors().size());
    Assertions.assertEquals(Cpf.of(CPF_SAMPLE), createdUser.getCpf());
  }

  @Test
  void givenInvalidNullCpf_whenCallNewUser_thenReturnListOfErrors() {
    final String expected_error_message = STRING_SHOULD_NOT_BE_NULL.replace("{}", CPF_STR);
    final Integer expected_error_count = 1;

    User user = User.of(USER_NAME_SAMPLE, null, true);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, user.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidBlankCpf_whenCallNewUser_thenReturnListOfErrors() {
    final Integer expected_error_count = 3;

    User user = User.of(USER_NAME_SAMPLE, "", true);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
  }

  @Test
  void givenInvalidFormatCpf_whenCallNewUser_thenReturnListOfErrors() {
    final String expected_cpf = "123";

    final Integer expected_error_count = 2;

    User user = User.of(USER_NAME_SAMPLE, expected_cpf, true);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
  }

  @Test
  void givenInvalidBlankCpfAndName_whenCallNewUser_thenReturnListOfErrors() {
    final Integer expected_error_count = 5;

    User user = User.of("", "", true);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
  }

}
