<center>
  <p align="center">
    <img src="https://gitlab.com/desafio-banco-inter-thales/user-manager/-/raw/main/public/logo.png" width="150"/>&nbsp;
  </p>  
  <h1 align="center">🚀 Microserviço: Gerenciador de usuários</h1>
  <p align="center">
    Microserviço referente ao desafio banco inter para criação de uma mini<br /> 
    plataforma de investimentos utilizando clean arch, DDD, 12 Factor App e as<br />
    boas práticas atuais do mercado. Espero que gostem!!
  </p>
</center>
<br />

## Objetivo do microserviço

De forma a tornar esse desafio mais simples não utilizei ferramentas mais sofisticadas
para validação de requisições ou mesmo autenticação e autorização. A idéia desse
microserviço é cadastrar dois tipos de usuários.

1. Usuários administradores

Aqueles que podem utilizar livremente o user-manager e o stock-manager

2. Usuários não administradores

Aqueles que só podem utilizar livremente o stock-manager

## Ferramentas necessárias para rodar o projeto em container

- Docker
- Docker compose

## Ferramentas necessárias para o projeto localmente

- IDE de sua preferência
- Navegador de sua preferência
- JDK 17
  - Utilizei o sdkman para instalação da versão **17.0.3-zulu**
- Docker e Docker compose
  - Para rodar as dependências
- Gradle
  - Utilizei o sdkman para instalação da versão **7.5**

## Pré-execução

1. Clonar o repositório:
```sh
git clone https://gitlab.com/desafio-banco-inter-thales/user-manager.git
```

2. Entre na pasta referente ao repositório:
```sh
cd user-manager
```

## Execução

> É possível executar o projeto de duas formas:

### Apenas containers

1. Execute o comando
```shell
docker-compose -f containers/docker-compose.yml up -d --build
```

2. Abra o navegador em http://localhost:8080/swagger-ui/index.html


### Aplicação rodando localmente

1. Execute o comando
```shell
docker-compose -f containers/docker-compose-deps.yml up -d --build
```
> A opção **--build** serve para não utilizarmos o cache para construção
> da imagem do user-manager, caso queira buildar uma imagem já tendo gerado
> a primeira e não quer perder tempo pode remover essa opção de --build

2. Execute a aplicação
```shell
./gradlew bootRun
```
> Também é possível executar como uma aplicação Java através do
> método main() na classe Main.java

3. Abra o navegador em http://localhost:8080/swagger-ui/index.html

## Banco de dados
O banco de dados principal é um H2 e para subir localmente não precisamos de 
passos extras, a própria aplicação se encarrega de subir as tabelas descritas
no arquivo presente em ```src/main/resources/data.sql```

> É importante mencionar que pela forma que o sistema foi escrito 
> toda vez que a aplicação é derrubada nossos registros serão removidos 
> do banco

## Stop

Caso queira derrubar a aplicação

1. Execute o comando
```shell
docker-compose -f containers/docker-compose.yml down
```

## Testes

1. Execute o comando
```shell
./gradlew test
```

> Também é possível executar via IDE. Utilizando o Intellij vá até a pasta
> **src/test/java/com/desafio** clique com o botão direito e na metade 
> das opções apresentadas escolha **Run Tests in 'com.desafio''**

## Responsabilidades do microserviço

- [x] Criação de usuários
- [x] Busca de um usuário específico
- [x] Listagem paginada dos usuários
- [x] Envio de um payload do usuário para o tópico ```USER_CREATED```

## Design do microserviço

Exemplo criação de usuário
<br/>
<center>
  <p align="center">
    <img src="https://gitlab.com/desafio-banco-inter-thales/user-manager/-/raw/main/public/desafio-UserMan-final.drawio.png"/>&nbsp;
  </p>  
</center>

### Características do microserviço

- [x] Utilizado comunicação síncrona na entrada de requisições
  - Para facilitar a criação de novos usuários
- [x] Utilizado comunicação assíncrona na saída de eventos
  - Spring cloud stream com binder do kafka
- [x] Utilizado H2 como banco de dados
- [x] Utilizado **Sleuth** para o tracing entre as aplicações
- [x] Utilizado **Zipkin** para visualização do tracing entre as aplicações
- [x] Docker para containerização
- [x] Code coverage com [sonarcloud ~ 82%](https://sonarcloud.io/project/overview?id=desafio-banco-inter-thales_user-manager)

#### Pontos negativos do microserviço

1. É uma alternativa muito simples a criação de uma service mesh ou api gateway.
2. Dependência muito forte do kafka sendo um single point of failure

##### kafka como single point of failure

Sobre isso o autor tem em mente uma possível solução algo como uma tabela
temporária na qual sempre que houvesse falha na conexão os registros iriam
para essa tabela. Quando houvesse o retorno do servidor do kafka então o job
enviaria em lotes ou todos os registros para serem consumidos pelo 
company-manager.